import React from 'react';
import './Contacts.css';
import Title from "../Title/Title";

const Contacts = () => {
  return (
    <div className="Contacts">
      <Title>Контакты</Title>
      <div className="ContactsContent">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Accusantium consequatur delectus enim laborum magnam maxime minima ratione repudiandae voluptas voluptatibus?</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Corporis culpa doloremque eius exercitationem laboriosam magnam neque pariatur repellendus saepe?
          A adipisci explicabo id ipsam numquam provident quaerat quod, recusandae ullam!</p>
        <p></p>
      </div>
    </div>
  );
};

export default Contacts;