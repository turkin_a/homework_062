import React from 'react';
import './BusinessCard.css';
import Btn from "../UI/Btn";
import {NavLink} from "react-router-dom";

const BusinessCard = (props) => {
  return (
    <div className="Card">
      <div className="CardPhoto-Wrap">
        <img className="CardPhoto" src="./img/Photo.jpg" alt=""/>
      </div>
      <div className="CardTitle">{props.title}</div>
      <p className="CardInfo">{props.info}</p>
      <NavLink to="/brief">
        <Btn BtnClass={'CardBtn'}>Подробнее</Btn>
      </NavLink>
    </div>
  );
};

export default BusinessCard;