import React from 'react';
import './NotFound.css';
import Title from "../Title/Title";

const NotFound = () => {
  return (
    <div className="NotFound">
      <Title>404 страница не найдена</Title>
    </div>
  );
};

export default NotFound;