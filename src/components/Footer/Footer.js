import React, { Component } from 'react';
import './Footer.css';
import NavBar from "../NavBar/NavBar";

class Footer extends Component {
  render() {
    return (
      <footer className="Footer">

        <NavBar />
      </footer>
    );
  }
}

export default Footer;