import React, { Component } from 'react';
import './MainMenu.css';
import NavBar from "../../NavBar/NavBar";

class MainMenu extends Component {
  render() {
    return (
      <div className="MainMenu">
        <NavBar />
      </div>
    );
  }
}

export default MainMenu;