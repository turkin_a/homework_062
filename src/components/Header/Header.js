import React, { Component } from 'react';
import './Header.css';
import MainMenu from "./MainMenu/MainMenu";

class Header extends Component {
  render() {
    return (
      <header className="Header">
        <div className="Logo"><h1><img src="./img/logo.png" alt="FuLion"/></h1></div>
        <MainMenu />
      </header>
    );
  }
}

export default Header;