import React, { Component, Fragment } from 'react';
import './FolioExamples.css';
import Title from "../Title/Title";
import FolioExamples from "./FolioExamples";

class Portfolio extends Component {
  render() {
    return (
      <Fragment>
        <Title>Портфолио</Title>
        <FolioExamples folios={this.props.folios} />
      </Fragment>
    );
  }
}

export default Portfolio;