import React, { Component } from 'react';
import './FolioExamples.css';

class FolioExamples extends Component {
  render() {
    return (
      <div className="FolioExamples">
        {this.props.folios.map(folio => {
          return (
            <div className="FolioItem" key={folio.name} >
              <img className="FolioItem-Img" src={'./img/folio_items/' + folio.name + '.jpg'} alt=""/>
              <p className="FolioItem-Description">{folio.description}</p>
            </div>
          );
        })}
      </div>
    );
  }
}

export default FolioExamples;