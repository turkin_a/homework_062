import React from 'react';
import './NavBar.css';
import {NavLink} from "react-router-dom";

const NavBar = () => {
  return (
    <ul className="Nav">
      <li><NavLink exact to="/" className="Nav-Item" activeClassName="Selected">Главная</NavLink></li>
      <li><NavLink to="/brief" className="Nav-Item" activeClassName="Selected">Резюме</NavLink></li>
      <li><NavLink to="/portfolio" className="Nav-Item" activeClassName="Selected">Портфолио</NavLink></li>
      <li><NavLink to="/contacts" className="Nav-Item" activeClassName="Selected">Контакты</NavLink></li>
    </ul>
  );
};

export default NavBar;