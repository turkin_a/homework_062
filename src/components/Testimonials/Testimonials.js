import React from 'react';
import './Testimonials.css';

const Testimonials = props => {
  return (
    <div className="Testimonials">
      <p>{props.info}</p>
    </div>
  );
};

export default Testimonials;