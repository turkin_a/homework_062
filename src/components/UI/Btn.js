import React from 'react';
import './Btn.css';

const Btn = props => {
  return (
    <button
      type="button"
      className={"Btn " + props.BtnClass}
    >{props.children}</button>
  );
};

export default Btn;