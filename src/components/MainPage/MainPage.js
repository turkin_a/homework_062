import React, { Component } from 'react';
import './MainPage.css';
import Title from "../Title/Title";
import BusinessCard from "../BusinessCard/BusinessCard";
import BorderLine from "../BorderLine";
import FolioExamples from "../Portfolio/FolioExamples";
import Testimonials from "../Testimonials/Testimonials";
import NavLink from "react-router-dom/es/NavLink";
import Btn from "../UI/Btn";

class MainPage extends Component {
  render() {
    return (
      <div className="Container">
        <div className="MainPage">
          <Title>Welcome!</Title>
          <BusinessCard
            title={'Алексей Туркин'}
            info={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus adipisci, ' +
            'reprehenderit, ab itaque, officiis praesentium modi ad repellendus eaque sunt eos laudantium quasi. ' +
            'Dolores sunt laboriosam sint perferendis voluptate doloribus, impedit magni ex debitis. ' +
            'Mollitia beatae tempora sint sapiente corporis.'}
          />
          <BorderLine />
          <Title>Portfolio</Title>
          <FolioExamples
            folios={[this.props.folios[0], this.props.folios[1], this.props.folios[9]]}
          />
          <NavLink to="/portfolio">
            <Btn BtnClass={'AllFolioBtn'}>Посмотреть все работы</Btn>
          </NavLink>
          <BorderLine />
          <Testimonials
            info={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. ' +
            'Magnam quasi pariatur illum at voluptatum tempore voluptate. ' +
            'Facere obcaecati maiores assumenda inventore nulla, illum excepturi sunt, neque eligendi! ' +
            'Debitis fugit facere quis vero, laudantium voluptatem. Quo vitae obcaecati modi fugit minima.'}
          />
        </div>
      </div>
    );
  }
}

export default MainPage;