import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

import MainPage from "../../components/MainPage/MainPage";
import Brief from "../../components/Brief/Brief";
import Portfolio from "../../components/Portfolio/Portfolio";
import Contacts from "../../components/Contacts/Contacts";
import NotFound from "../../components/NotFound/NotFound";

class Main extends Component {
  state = {
    folios: [
      {name: 'Homework_018', description: 'HTML+CSS: Animation'},
      {name: 'Homework_038', description: 'jQuery: Date picker'},
      {name: 'Homework_040', description: 'jQuery: Plugins'},
      {name: 'Homework_042', description: 'jQuery: Promises'},
      {name: 'Exam_007', description: 'React: Order list constructor'},
      {name: 'Homework_052', description: 'React: Sorting'},
      {name: 'Homework_054', description: 'React: Poker'},
      {name: 'Homework_055', description: 'React: Finding item'},
      {name: 'Homework_056', description: 'React: PiggyBank'},
      {name: 'Homework_061', description: 'React: Promises'}
    ]
  };

  render() {
    return (
      <div className="Container">
        <Header />
        <Switch>
          <Route path="/" exact render={(props) => <MainPage {...props} folios={this.state.folios} />} />
          <Route path="/brief" component={Brief} />
          <Route path="/portfolio" render={(props) => <Portfolio {...props} folios={this.state.folios} />} />
          <Route path="/contacts" component={Contacts} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default Main;